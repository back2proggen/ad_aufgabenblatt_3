package LinkedList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import Generics.TestElement;

public class LinkedListTest {

	@Test
	public void testInsertundFind() {
		
		LinkedList testList = new LinkedList();
		
		testList.insert(testList.getHead(), new TestElement(0));// h e0 t
		assertEquals(1,testList.getListenLange());
		assertEquals(testList.find(0),testList.getHead());
		
		testList.insert(testList.getHead(), new TestElement(5));//h e5 e0 t
		assertEquals(2,testList.getListenLange());
		assertEquals(testList.find(0),testList.getHead().getNext());
		
		testList.insert(testList.find(0).getNext(), new TestElement(10));//h e5 e0 e10 t
		assertEquals(3,testList.getListenLange());
		assertEquals(testList.find(10),testList.find(0).getNext());
		
		assertEquals(testList.getTail().getNext(), testList.find(10).getNext());
		
		//Keine Position angegeben Fehler
		try{
			testList.insert(null, new TestElement(7));
			fail();
			}
			catch(Exception e){
			}
		
		// Key schon vorhanden, fehler
		try{
			testList.insert(testList.getHead(), new TestElement(10));
			fail();
			}
			catch(Exception e){
			}
		//Element nicht vorhanden, Methode liefert 0 wenn nur Tail gefunden wird
		assertEquals(null,testList.find(33)); 
		
		
	}

	@Test
	public  void testDelete() {
		LinkedList testList = new LinkedList();
		
		testList.insert(testList.getHead(), new TestElement(0));// h e0 t
		testList.insert(testList.getHead(), new TestElement(5));//h e5 e0 t		
		testList.insert(testList.find(0).getNext(), new TestElement(10));//h e5 e0 e10 t
		assertEquals(3,testList.getListenLange());
		
		testList.deleteKey(0);//h e5 e10 t
		assertEquals(2,testList.getListenLange());
		assertEquals(testList.find(10),testList.find(5).getNext());
		
		testList.insert(testList.getHead(), new TestElement(15));//h e15 e5 e10 t
		testList.insert(testList.getHead(), new TestElement(20));//h e20 e15 e5 t		
		testList.insert(testList.getHead(), new TestElement(30));//h e30 e20 e15 e5 e10 t
		testList.deletePos(testList.getHead());
		assertEquals(testList.find(20), testList.getHead());
		
		assertEquals(4,testList.getListenLange());
		
		try{
			testList.deletePos(null);;
			fail();
			}
			catch(Exception e){
			}
		
		testList.deleteKey(241);
		
		assertEquals(4,testList.getListenLange());

		
	}

	@Test
	public  void testConcatundRetrieve() {
		LinkedList testList = new LinkedList();
		
		testList.insert(testList.getHead(), new TestElement(0));// h e0 t
		testList.insert(testList.getHead(), new TestElement(5));//h e5 e0 t		
		testList.insert(testList.find(0).getNext(), new TestElement(10));//h e5 e0 e10 t
		
		LinkedList testList2 = new LinkedList();
		
		testList2.insert(testList2.getHead(), new TestElement(15));// h e15 t
		testList2.insert(testList2.getHead(), new TestElement(20));//h e20 e15 t		
		testList2.insert(testList2.find(20).getNext(), new TestElement(25));//h e20 e25 e15  t
		
		assertEquals(20, testList2.retrieve(testList2.find(25)).getKey());
		
		
		testList.concat(testList2);
		
		assertEquals(6,testList.getListenLange());
		assertEquals(0,testList2.getListenLange());
		
		assertEquals(testList.find(15).getNext(),testList.getTail().getNext());
	}
	
//	@Test
//	public static void aufwandsAnalyse(){
//}
		
}