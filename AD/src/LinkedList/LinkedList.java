package LinkedList;

import Generics.*;

public class LinkedList implements Liste {

	private Knoten head;
	private Knoten tail;
	private int listLength;

	private double counter = 0;

	public double getCounter() {
		double counterZahl = counter;
		counter = 0;
		return counterZahl;
	}

	public Knoten getHead() {
		return head;
	}

	public Knoten getTail() {
		return tail;
	}

	/**
	 * Leert die Liste.
	 */
	public void Clear() {
		head = new Knoten(null, null, null);
		tail = new Knoten(null, head, null);
		head.setNext(tail);
		listLength = 0;
		// Counter fuer Aufwandsanalyse
	}

	/**
	 * Initialisierung der LinkedList. Am anfang wird der setNext von dem Kopf
	 * mit dem Tail verknp�ft, da noch keien Elemente vorhanden sind.
	 */
	public LinkedList() {
		head = new Knoten();
		tail = new Knoten(head, null);
		head.setNext(tail);
		listLength = 0;
		// Counter fuer Aufwandsanalyse
	}

	/**
	 * F�gt ein Element an einer bestimmten position ein.
	 */
	@Override
	public void insert(Knoten position, Element element) {

		// Counter fuer Aufwandsanalyse
		counter += 3;
		if (find(element.getKey()) != null) {
			throw new IndexOutOfBoundsException();
		}
		// Counter fuer Aufwandsanalyse
		counter++;
		if (position == null) {
			throw new IndexOutOfBoundsException();
		}
		// Counter fuer Aufwandsanalyse
		counter += 3;
		//Das einzuf�gende Element wird mit der Position des n�chsten Elementes eingef�gt.
		//Als next f�r das neue element wird das next der momentanen position eingetragen
		//das next des neuen elements zeigt auf das alte element an dieser position
		if (position != tail && position.getNext() != null) {
			Knoten tmp = new Knoten(position.getNext(), element);
			position.setNext(tmp);
			if (tmp.getNext() == tail) {
				tail.setNext(tmp);
				counter++;
			}
			listLength++;
			// Counter fuer Aufwandsanalyse
			counter += 7;
		} else {
			counter++;
			// Falls die position tail ist,wird ein neues element vor dem tail eingef�gt
			if (position == tail) {
				Knoten tmp = new Knoten(tail, element);
				tail.getNext().setNext(tmp);
				tail.setNext(tmp);
				listLength++;
				// Counter fuer Aufwandsanalyse
				counter += 6;
			} else {
				position.setElement(element);
				position.setNext(tail);
				tail.getNext().setNext(position);
				tail.setNext(position);
				listLength++;
				// Counter fuer Aufwandsanalyse
				counter += 6;
			}
		}

	}


	@Override
	public void deletePos(Knoten pos) {
		counter++;
		if (pos != null) {
			pos.setNext(pos.getNext().getNext());
			listLength--;
			// Counter fuer Aufwandsanalyse
			counter += 4;
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	/**
	 * Ein Schl�ssel wird �bergeben um das Element, welches zu diesem Schl�ssel
	 * geh�rt zu l�schen.
	 */
	@Override
	public void deleteKey(int key) {
		// Counter fuer Aufwandsanalyse
		counter += 3;
		Knoten gefunden = find(key);
		if (gefunden == null) {
			return;
		} else {
			deletePos(find(key));
			// Counter fuer Aufwandsanalyse
			counter += 2;
		}

	}

	/**
	 * Durchsucht mit einem Schl�ssel die Liste, der Tail der Liste wird mit
	 * einem Element bef�hlt, der den Key enth�lt. Falls der Key nicht
	 * auffindbar ist wird die position wird null zur�ckgegeben. F�r das
	 * erf�llen der antizipativen indizierung, wird die Position des Elements vor dem gesuchten Element zur�ckgegeben.
	 */
	@Override
	public Knoten find(int key) {
		Knoten pos = head;
		tail.setElement(new TestElement(key));
		// Counter fuer Aufwandsanalyse
		counter += 3;
		while (pos.getNext().getElement().getKey() != key) {
			pos = pos.getNext();
			// Counter fuer Aufwandsanalyse
			counter += 6;
		}

		tail.setElement(null);
		// Counter fuer Aufwandsanalyse
		counter += 2;
		if (pos.getNext() == tail) {
			return null;
		}
		return pos;
	}

	@Override
	public Element retrieve(Knoten pos) {
		// Counter fuer Aufwandsanalyse
		counter += 2;
		if (pos.getElement() != null) {
			// Counter fuer Aufwandsanalyse
			counter++;
			return pos.getElement();

		} else {
			return null;
		}
	}

	/**
	 * Verkn�pft 2 Listen miteinander und l�scht die alte Liste
	 */
	@Override
	public void concat(Liste liste2) {
		counter++;
		if (liste2 instanceof LinkedList) {
			LinkedList tmp = (LinkedList) liste2;
			tail.getNext().setNext(tmp.getHead().getNext());
			tail = tmp.getTail();
			listLength += tmp.getListenLange();
			// Counter fuer Aufwandsanalyse
			counter += 10;
			tmp.Clear();

		}

	}

	public int getListenLange() {
		return listLength;
	}
}
