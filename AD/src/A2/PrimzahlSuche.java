package A2;


	public class PrimzahlSuche {
		
		//langesame suche
		public void primzahlenSuche(int N) {

			boolean[] a = new boolean[N];
			for (int i = 0; i < N; i++) {
				a[i] = true;
			}
			
			for (int i = 2; i < N; i++) {
				AufwandsAnalyse.counter++;
				for (int j = 2; j < N; j++) {
					AufwandsAnalyse.counter++;
					if ((i % j == 0) && (j != i)){
						a[i] = false;
					}
				}
			}
		}

		public void primzahlenSucheSchnell(int N) {
			boolean[] a = new boolean[N];
			for (int i = 0; i < N; i++) {
				if(i%2 == 0){
					a[i] = false;
				}
				else{
					a[i] = true;
				}
			}
			for (int i = 3; i < N; i+=2) {
				AufwandsAnalyse.counter++;
				for (int j = 3; j < Math.sqrt(i); j+=2) {
					AufwandsAnalyse.counter++;
					if ((i % j == 0)) {
						a[i] = false;
						break;
					}
				}
			}
		}

		// Sieb
		public void primzahlenSucheSieb(int N) {
			boolean[] a = new boolean[N];
			for (int i = 0; i < N; i++) {
				a[i] = true;
			}
			
			for (int i = 2; i < Math.sqrt(N); i++) {
				AufwandsAnalyse.counter++;
				if (a[i] == true)
					for (int j = 2; i * j < N; j++) {
						AufwandsAnalyse.counter++;
						a[i * j] = false;
					}
			}
		}
		
		public boolean istPrimzahl(int N){
			AufwandsAnalyse.counter++;
			if(N%2==0){
				return false;
			}
			for(int i = 3; i < Math.sqrt(N); i+=2){
				AufwandsAnalyse.counter++;
				if(N%i==0){
					return false;
				}
			}
			return true;
		}
		
		
	}
	
	


