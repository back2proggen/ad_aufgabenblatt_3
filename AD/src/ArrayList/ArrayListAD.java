package ArrayList;
import Generics.*;

public class ArrayListAD implements Liste {

	private int anzahlElemente;
	private Element[] elemente;
	private double counter = 0;


	public ArrayListAD() {
		elemente = new Element[10];	
		counter++;
	}
	
	public double getCounter(){
		double counterZahl = counter;
		counter = 0;
		return counterZahl;
	}

	/**
	 * f�gt ein element an der entsprechenden Position
	 * 
	 * @param pos Die Position des Elementes
	 * @param element das hinzugef�gt werden soll
	 */
	@Override
	public void insert(Knoten pos, Element element) {
		counter++;
		//Testet ob der key schon existiert
		int keyLoc = find(element.getKey()).getArrayIndex();
		if(keyLoc != -1){
			throw new IndexOutOfBoundsException();
		}
		counter+=5;
		pos.setArrayIndex(pos.getArrayIndex()+1);
		if(pos.getArrayIndex() > getElemente().length || pos.getArrayIndex() < 1){
			throw new IndexOutOfBoundsException();
		}
		counter+=8;

		
		if (anzahlElemente >= elemente.length-1) {
			Element[] tmp = elemente;
			counter+=anzahlElemente;
			elemente = new Element[anzahlElemente*2];
			System.arraycopy(tmp, 0, elemente, 0, tmp.length);
			counter+=4;

		}
		for (int j = elemente.length-1; j >= pos.getArrayIndex(); j--) {
			counter+=3;
			if (elemente[j] != null) {
				elemente[j + 1] = elemente[j];
				counter+=2;
			}
		}
		elemente[pos.getArrayIndex()] = element;
		anzahlElemente++;
		counter+=2;
	}

	public void setElemente(Element[] elemente) {
		this.elemente = elemente;
	}

	/**
	 * entfernt ein Element aus der Liste anhand der Position
	 * 
	 * @param pos Die Position des Elementes
	 */
	@Override
	public void deletePos(Knoten pos) {
		pos.setArrayIndex(pos.getArrayIndex()+1);
				if(pos == null || pos.getArrayIndex() > getElemente().length || pos.getArrayIndex() < 1){
					throw new IndexOutOfBoundsException();
				}
				counter+=8;

		if (elemente[pos.getArrayIndex()] != null) {
			elemente[pos.getArrayIndex()] = null;
			anzahlElemente--;
			counter+=6;

			for (int j = pos.getArrayIndex(); j < elemente.length - 1; j++) {
				if (elemente[j + 1] != null) {
					elemente[j] = elemente[j + 1];
					elemente[j + 1] = null;
					counter+=3;
				}
				counter+=2;

			}
		}
	}

	/**
	 * entfernt ein Element aus der Liste
	 * 
	 * @param key Das Element,welches in der Liste zu entfernen ist
	 */
	@Override
	public void deleteKey(int key) {
		int k = find(key).getArrayIndex() + 1;
		if (k == 0) {
			return;
		}
		counter+=5;

		elemente[find(key).getArrayIndex() + 1] = null;
		counter+=4;
		for (int j = k; j <= (anzahlElemente - k) + 1; j++) {
			elemente[j] = elemente[j + 1];
			counter+=3;
		}
		anzahlElemente--;
		counter++;
	}

	/**
	 * liefert die Position des zu suchenden Element
	 * 
	 * @param key Das Element was gesucht werden soll
	 */
	@Override
	public Knoten find(int key) {
		Element element = new TestElement(key);
		elemente[0] = element;
		Knoten pos = new Knoten();
		pos.setArrayIndex(anzahlElemente);
		counter+=5;
		Element element2 =  elemente[pos.getArrayIndex()];

		while (element2.getKey() != element.getKey()) {
			pos.setArrayIndex(pos.getArrayIndex() - 1);
			element2 =  elemente[pos.getArrayIndex()];
			counter+=8;
		}
		elemente[0] = null;
		pos.setArrayIndex(pos.getArrayIndex() - 1);
		counter+=3;
		return pos;
	}

	/**
	 * holt ein Element aus der Liste
	 * 
	 * @param pos Die Position des Element
	 */
	@Override
	public Element retrieve(Knoten pos) {
		if(pos == null){
			throw new IndexOutOfBoundsException();
		}
		counter++;
		pos.setArrayIndex(pos.getArrayIndex()+1);
		counter+=4;
		return (Element) elemente[pos.getArrayIndex()];
	}

	/**
	 * Fuegt die liste kette an der liste die diese methode aufruft zu
	 * 
	 * @param kette die liste die angeh�ngt werden soll
	 */
	@Override
	public void concat(Liste kette) {
		if (kette instanceof ArrayListAD) {
			ArrayListAD addList = (ArrayListAD) kette;
			if (anzahlElemente + addList.getListenLange() >= elemente.length) {
				Element[] tmp = elemente;
				elemente = new Element[anzahlElemente + addList.getListenLange()+1];
				counter+=8;
				System.arraycopy(tmp, 0, elemente, 1, tmp.length);
				counter+=tmp.length;
				System.arraycopy(addList.getElemente(), 1, elemente, anzahlElemente + 1, addList.getListenLange());
				counter+=addList.getListenLange();
				anzahlElemente = anzahlElemente+addList.getListenLange();
				counter+=3;
			} else {
				System.arraycopy(addList.getElemente(), 1, elemente, anzahlElemente + 1, addList.getListenLange());
				counter+=addList.getListenLange();
				anzahlElemente = anzahlElemente+addList.getListenLange();
				counter+=3;
			}
		}

	}

	@Override
	public int getListenLange() {
		return anzahlElemente;
	}

	public Object[] getElemente() {
		return elemente;
	}
}
