package ArrayList;

import Generics.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayListADTest {
	@Test
	public void testInsertundFind() {

		ArrayListAD testList = new ArrayListAD();
		Knoten[] testPosition = new Knoten[] { new Knoten(), new Knoten(), new Knoten() };

		testPosition[0].setArrayIndex(0);
		testList.insert(testPosition[0], new TestElement(0));// h e0 t
		assertEquals(1, testList.getListenLange());
		assertEquals(0, testList.find(0).getArrayIndex());

		testPosition[1].setArrayIndex(1);
		testList.insert(testPosition[1], new TestElement(5));// h e0 e5 t
		assertEquals(2, testList.getListenLange());
		assertEquals(1, testList.find(5).getArrayIndex());

		testPosition[0].setArrayIndex(0);
		testList.insert(testPosition[0], new TestElement(10));// h e10 e0 e5 t
		assertEquals(3, testList.getListenLange());
		assertEquals(0, testList.find(10).getArrayIndex());
		assertEquals(2, testList.find(5).getArrayIndex());

		// Keine Position angegeben Fehler
		try {
			testList.insert(null, new TestElement(7));
			fail();
		} catch (Exception e) {
		}

		// Key schon vorhanden, fehler
		try {
			Knoten knoten = new Knoten();
			knoten.setArrayIndex(0);
			testList.insert(knoten, new TestElement(10));
			fail();
		} catch (Exception e) {
		}
		// Element nicht vorhanden, Methode liefert -1
		assertEquals(-1, testList.find(33).getArrayIndex());
	}

	@Test
	public void testDelete() {
		ArrayListAD testList = new ArrayListAD();
		Knoten[] testPosition = new Knoten[] { new Knoten(0), new Knoten(1), new Knoten(2), new Knoten(3) };

		testList.insert(testPosition[0], new TestElement(0));// h e0 t
		testList.insert(testPosition[1], new TestElement(5));// h e0 e5 t
		testList.insert(testPosition[2], new TestElement(10));// h e0 e5 e10 t

		assertEquals(3, testList.getListenLange());

		testList.deleteKey(0);// h e5 e10 t

		assertEquals(2, testList.getListenLange());
		assertEquals(0, testList.find(5).getArrayIndex());

		testPosition[2].setArrayIndex(2);
		testPosition[1].setArrayIndex(1);

		testList.insert(testPosition[2], new TestElement(0));// h e5 e10 e0 t
		testList.insert(testPosition[3], new TestElement(20));// h e5 e10 e0 e20
																// t
		testList.deletePos(testPosition[1]);// h e5 e0 e20 t

		testPosition[1].setArrayIndex(1);
		assertEquals(testPosition[1].getArrayIndex(), testList.find(0).getArrayIndex());
		assertEquals(-1, testList.find(10).getArrayIndex());
		assertEquals(3, testList.getListenLange());

		try {
			testList.deletePos(null);
			fail();
		} catch (Exception e) {
		}

		testList.deleteKey(124);

		assertEquals(3, testList.getListenLange());

	}

	@Test
	public void testConcatundRetrieve() {
		ArrayListAD testList = new ArrayListAD();

		TestElement[] testElemente = new TestElement[] { new TestElement(0), new TestElement(5), new TestElement(10) };

		testList.insert(new Knoten(0), testElemente[0]);// h e0 t
		testList.insert(new Knoten(1), testElemente[1]);// h e0 e5 t
		testList.insert(new Knoten(2), testElemente[2]);// h e0 e5 e10 t

		assertEquals(testElemente[0].getKey(), testList.retrieve(new Knoten(0)).getKey());
		assertEquals(testElemente[2].getKey(), testList.retrieve(new Knoten(2)).getKey());

		ArrayListAD testList2 = new ArrayListAD();
		TestElement[] testElemente2 = new TestElement[] { new TestElement(15), new TestElement(20),
				new TestElement(25) };

		testList2.insert(new Knoten(0), testElemente2[0]);// h e15 t
		testList2.insert(new Knoten(1), testElemente2[1]);// h e15 e20 t
		testList2.insert(new Knoten(2), testElemente2[2]);// h e15 e20 e25 t

		assertEquals(testElemente2[0].getKey(), testList2.retrieve(new Knoten(0)).getKey());
		assertEquals(testElemente2[2].getKey(), testList2.retrieve(new Knoten(2)).getKey());

		try {
			testList.retrieve(null);
			fail();
		} catch (Exception e) {

		}

		assertEquals(null, testList.retrieve(new Knoten(-1)));

		testList.concat(testList2);

		assertEquals(6, testList.getListenLange());
		assertEquals(3, testList2.getListenLange());

		assertEquals(testElemente[0].getKey(), testList.retrieve(new Knoten(0)).getKey());
		assertEquals(testElemente2[2].getKey(), testList.retrieve(new Knoten(5)).getKey());
	}
}
