package DoubleLinkedList;

import Generics.Knoten;
import Generics.TestElement;

public class AufwandsAnalyse {

	private int listenLange;
	private DoubleLinkedList analysenListe;
	
	public AufwandsAnalyse(int listenGroesse){
		analysenListe = new DoubleLinkedList();
		this.listenLange = listenGroesse;
	}
	
	public void insertAnalyse() {
		for (int i = 1; i < listenLange; i++) {
			analysenListe.insert(new Knoten(), new TestElement(i));
		}
		System.out.println("Erstelle Liste mit "+listenLange+" Elementen. Counter: " + analysenListe.getCounter());
	}
	
	private Knoten keyPosition;
	public void findAnalyse() {

		keyPosition = analysenListe.find(listenLange/2);
		System.out.println("Methode find() fuer element mit dem key "+listenLange/2+" :Counter " + analysenListe.getCounter());
	}
	
	public void deleteKeyAnalyse() {

		analysenListe.deleteKey(listenLange/2);
		System.out.println("Methode deleteKey() fuer element mit dem key "+listenLange/2+" :Counter " + analysenListe.getCounter());
	}
	
	public void retrieveAnalyse() {

		analysenListe.retrieve(keyPosition);
		System.out.println("Methode retrieve() fuer element mit dem key "+listenLange/2+" : Counter " + analysenListe.getCounter());
	}
	
	public void concatAnalyse() {

		analysenListe.find(5);
		System.out.println("Methode find() fuer element mit dem key "+listenLange/2+" : Counter " + analysenListe.getCounter());
	}

	public static void main(String[] args) {
		// 10 elemente
		System.out.println("10\n");
		AufwandsAnalyse aufwand = new AufwandsAnalyse(10);

		aufwand.insertAnalyse();
		aufwand.findAnalyse();
		aufwand.retrieveAnalyse();
		aufwand.deleteKeyAnalyse();

		// 100 elemente
		System.out.println("100\n");
		aufwand = new AufwandsAnalyse(100);

		aufwand.insertAnalyse();
		aufwand.findAnalyse();
		aufwand.retrieveAnalyse();
		aufwand.deleteKeyAnalyse();

		// 1000 elemente
		System.out.println("1000\n");
		aufwand = new AufwandsAnalyse(1000);

		aufwand.insertAnalyse();
		aufwand.findAnalyse();
		aufwand.retrieveAnalyse();
		aufwand.deleteKeyAnalyse();

		// 10000 elemente
		System.out.println("10000\n");
		aufwand = new AufwandsAnalyse(10000);

		aufwand.insertAnalyse();
		aufwand.findAnalyse();
		aufwand.retrieveAnalyse();
		aufwand.deleteKeyAnalyse();

		// 100000 elemente
		System.out.println("100000\n");
		aufwand = new AufwandsAnalyse(100000);

		aufwand.insertAnalyse();
		aufwand.findAnalyse();
		aufwand.retrieveAnalyse();
		aufwand.deleteKeyAnalyse();
	}

}
