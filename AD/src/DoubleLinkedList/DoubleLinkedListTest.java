package DoubleLinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import Generics.Knoten;
import Generics.TestElement;

public class DoubleLinkedListTest {

	@Test
	public void testInsertundFind(){
		
		DoubleLinkedList testList = new DoubleLinkedList();
		Knoten[] testPosition = new Knoten[]{new Knoten(),new Knoten(),new Knoten()};
		
		testList.insert(testPosition[0], new TestElement(0));// h e0 t
		assertEquals(1,testList.getListenLange());
		assertEquals(testPosition[0],testList.find(0));
		
		testList.insert(testPosition[1], new TestElement(5));//h e0 e5 t
		assertEquals(2,testList.getListenLange());
		assertEquals(testPosition[0],testList.find(5).getPrevious());
		
		testList.insert(testPosition[0], new TestElement(10));//h e10 e0 e5 t
		assertEquals(3,testList.getListenLange());
		assertEquals(testPosition[0],testList.find(10));
		assertEquals(testPosition[1],testList.find(10).getNext().getNext());
		
		//Keine Position angegeben Fehler
		try{
			testList.insert(null, new TestElement(7));
			fail();
			}
			catch(Exception e){
			}
		
		// Key schon vorhanden, fehler
		try{
			testList.insert(new Knoten(), new TestElement(10));
			fail();
			}
			catch(Exception e){
			}
		//Element nicht vorhanden, Methode liefert 0 wenn nur Tail gefunden wird
		assertEquals(null,testList.find(33));
	}

	@Test
	public  void testDelete() {
		DoubleLinkedList testList = new DoubleLinkedList();
		Knoten[] testPosition = new Knoten[]{new Knoten(),new Knoten(),new Knoten(), new Knoten()};
		
		testList.insert(testPosition[0], new TestElement(0));// h e0 t
		testList.insert(testPosition[1], new TestElement(5));//h e0 e5 t		
		testList.insert(testPosition[2], new TestElement(10));//h e0 e5 e10 t
		
		assertEquals(3,testList.getListenLange());
		
		testList.deleteKey(0);//h e5 e10 t
		
		assertEquals(2,testList.getListenLange());
		assertEquals(testList.getHead(),testList.find(5).getPrevious());
		
		testList.insert(testPosition[0], new TestElement(0));//h e5 e10 e0 t
		testList.insert(testPosition[3], new TestElement(20));//h e5 e10 e0 e20 t
		testList.deletePos(testPosition[2]);//h e5 e0 e20 t
		
		assertEquals(testPosition[1], testList.find(0).getPrevious());
		assertEquals(null,testList.find(10));
		assertEquals(3,testList.getListenLange());
		
		try {
			testList.deletePos(null);
			;
			fail();
		} catch (Exception e) {
		}
		
		try {
			testList.deletePos(testPosition[2]);
			fail();
		} catch (Exception e) {
		}
		
		testList.deleteKey(124);
		
		assertEquals(3,testList.getListenLange());

		
	}

	@Test
	public  void testConcatundRetrieve() {
		DoubleLinkedList testList = new DoubleLinkedList();
		
		TestElement[] testElemente = new TestElement[]{new TestElement(0),new TestElement(5),new TestElement(10)};
		
		testList.insert(new Knoten(), testElemente[0]);// h e0 t
		testList.insert(new Knoten(), testElemente[1]);//h e0 e5 t		
		testList.insert(new Knoten(), testElemente[2]);//h e0 e5 e10 t
		
		assertEquals(testElemente[0],testList.retrieve(testList.getHead().getNext()));
		assertEquals(testElemente[2],testList.retrieve(testList.getTail().getPrevious()));
		
		DoubleLinkedList testList2 = new DoubleLinkedList();
		TestElement[] testElemente2 = new TestElement[]{new TestElement(15),new TestElement(20),new TestElement(25)};
		
		testList2.insert(new Knoten(), testElemente2[0]);// h e15 t
		testList2.insert(new Knoten(), testElemente2[1]);//h e15 e20 t		
		testList2.insert(new Knoten(), testElemente2[2]);//h e15 e20 e25 t
		
		assertEquals(testElemente2[0],testList2.retrieve(testList2.getHead().getNext()));
		assertEquals(testElemente2[2],testList2.retrieve(testList2.getTail().getPrevious()));
		
		try{
			testList.retrieve(null);
			fail();
		}catch(Exception e){
			
		}
		
		assertEquals(null,testList.retrieve(new Knoten()));
		
		testList.concat(testList2);
		
		assertEquals(6,testList.getListenLange());
		assertEquals(0,testList2.getListenLange());
		
		assertEquals(testElemente[0],testList.retrieve(testList.getHead().getNext()));
		assertEquals(testElemente2[2],testList.retrieve(testList.getTail().getPrevious()));
	}
//	
////	@Test
////	public static void aufwandsAnalyse(){
////}
//		
}