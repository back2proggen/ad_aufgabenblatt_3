package DoubleLinkedList;
import Generics.*;

public class DoubleLinkedList implements Liste {
	
	private Knoten tail;
	private Knoten head;
	private int listenLange;
	
	private double counter;
	public double getCounter(){
		double returnCounter = counter;
		counter = 0;
		return returnCounter;
	}
	
	public Knoten getHead(){
		return head;
	}
	public Knoten getTail(){
		return tail;
	}
	public void Clear(){
		head = new Knoten(null,null,null);
		tail = new Knoten(null,head,null);
		head.setNext(tail);
		listenLange = 0;
	}
	
	public DoubleLinkedList(){
		head = new Knoten(null,null,null);
		tail = new Knoten(null,head,null);
		head.setNext(tail);
		listenLange = 0;
	}

	@Override
	public void insert(Knoten pos, Element element) {
		
		counter+=5;
		if(pos == null || pos == tail || find(element.getKey()) != null|| pos == head){
			throw new IndexOutOfBoundsException();
		}
		
		counter+=2;
		if(pos.getPrevious() != null){
			Knoten tmp = new Knoten(pos.getNext(),pos,pos.getElement());
			tmp.getNext().setPrevious(tmp);
			pos.setNext(tmp);
			pos.setElement(element);
			listenLange++;
			counter+=9;
		}
		else{
			pos.setElement(element);
			pos.setNext(tail);
			pos.setPrevious(tail.getPrevious());
			tail.getPrevious().setNext(pos);
			tail.setPrevious(pos);
			listenLange++;
			counter+=8;
		}
		
	}

	@Override
	public void deletePos(Knoten pos) {
		counter+=5;
		if(pos == null || pos == tail || pos == head || pos.getElement() == null){
			throw new IndexOutOfBoundsException();
		}
		
		pos.getPrevious().setNext(pos.getNext());
		pos.getNext().setPrevious(pos.getPrevious());
		pos.setNext(null);
		pos.setPrevious(null);
		pos.setElement(null);
		listenLange--;
		counter +=10;
	}

	@Override
	public void deleteKey(int key) {
		counter+=3;
		Knoten gefunden = find(key);
		if(gefunden == null){
			return;
		}
		counter++;
		deletePos(gefunden);
	}

	@Override
	public Knoten find(int key) {
		counter+=4;
		Knoten tmp = head.getNext();
		tail.setElement(new TestElement(key));
		
		while(tmp.getElement().getKey() != key){
			tmp = tmp.getNext();
			counter+=5;
		}
		counter++;
		if(tmp == tail){
			return null;
		}
		return tmp;
	}

	@Override
	public Element retrieve(Knoten pos) {
		counter+=3;
		if(pos == null || pos == tail || pos == head){
			throw new IndexOutOfBoundsException();
		}
		counter++;
		return pos.getElement();
	}

	@Override
	public void concat(Liste liste2) {
		counter++;
		if(liste2 instanceof DoubleLinkedList){
			DoubleLinkedList tmp = (DoubleLinkedList)liste2;
			tail.getPrevious().setNext(tmp.getHead().getNext());
			tmp.getHead().getNext().setPrevious(tail.getPrevious());
			tail = tmp.getTail();
			listenLange+= tmp.getListenLange();
			tmp.Clear();
			counter+=16;
		}
	}

	@Override
	public int getListenLange() {
		return listenLange;
	}
	
	
}
