package Generics;

public class Knoten {
	private Element element;
	private Knoten next;
	private Knoten previous;
	private int arrayIndex;

	public Knoten(Knoten next,Knoten previous,Element element){
		this.next = next;
		this.previous = previous;
		this.element = element;
		arrayIndex = 0;
	}
	public Knoten(Knoten next,Element element){
		this.next = next;
		this.previous = null;
		this.element = element;
		arrayIndex = 0;
	}
	public Knoten(int arrayIndex,Element element){
		this.next = null;
		this.previous = null;
		this.element = element;
		this.arrayIndex = arrayIndex;
	}
	public Knoten(){
		this.next = null;
		this.previous = null;
		this.element = null;
		arrayIndex = 0;
	}
	public Knoten(int arrayIndex){
		this.next = null;
		this.previous = null;
		this.element = null;
		this.arrayIndex = arrayIndex;
	}
	public int getArrayIndex(){
		return arrayIndex;
	}
	public void setArrayIndex(int arrayIndex){
		this.arrayIndex = arrayIndex;
	}
	
	public Knoten getNext(){
		return next;
	}
	public void setNext(Knoten next){
		this.next = next;
	}
	
	public Knoten getPrevious(){
		return previous;
	}
	public void setPrevious(Knoten previous){
		this.previous = previous;
	}
	
	public Element getElement(){
		return element;
	}
	public void setElement(Element element){
		this.element = element;
	}
}
