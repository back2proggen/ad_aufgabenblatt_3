package Generics;

public class TestElement implements Element{

	private int key;
	
	public TestElement(int key){
		this.key = key;
	}
	
	@Override
	public int getKey() {
		return key;
	}
	
}
