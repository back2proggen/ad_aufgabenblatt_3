package Generics;


public interface Liste{
	/**
	 * Fugt ein neues element der liste hiznu auf position pos
	 * @param die position wo wir das neue element hinzufugen wollen befindet sich zwischen 0 und listenLenge
	 * @param element das element das wir hinzufugen wollen
	 * Wenn sich pos auserhalb der liste ist wird eine IndexOutOfBound exception geworfen
	 */
	void insert(Knoten pos,Element element);
	/**
	 * Loscht das element an der position pos
	 * @param pos die position des elementes welches wir entfernen wollen, befindet sich zwischen 0 und listenLenge-1
	 * Wenn sich pos auserhalb der liste befindet wird eine IndexOutOfBound exception geworfen
	 */
	void deletePos(Knoten pos);
	/**
	 * Loscht das element mit dem key key
	 * @param key
	 */
	void deleteKey(int key);
	/**
	 * 
	 * @param key
	 * @return die position in der liste
	 */
	Knoten find(int key);
	/**
	 * Gibt das element zuruck das sich an der position pos befindet
	 * @param pos die position des elementes welches wir suchen
	 * @return das element das sich an der position pos befindet
	 * wenn sich pos auserhalb der liste befindet wird ein IndexOutOfBound exception geworfen
	 */
	Element retrieve(Knoten pos);
	/**
	 * 
	 * @param kette das element das an der liste angefugt wirdt
	 */
	void concat(Liste liste2);
	
	int getListenLange();
	
	
}
